#==============================================================================
#
# This file is part of the SoftPosit Math Package
# by S. H. Leong (Cerlane), John Gustafson and Jonathan Low.
#
# Copyright 2019 A*STAR.  All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#===============================================================================

CC = gcc
SOURCE_DIR ?= ../source


ifeq ($(SOFTPOSIT),)
	SOFTPOSIT:=../../SoftPosit-master
endif


SPSOURCE_DIR ?= $(SOFTPOSIT)/source
SPBUILD_DIR ?= $(SPSOURCE_DIR)/../build/Linux-x86_64-GCC

ifeq ($(OS),Windows_NT)
     DELETE = del /Q /F
else
     DELETE = rm -f
endif

C_INCLUDES = -I../include -I$(SPBUILD_DIR) -I$(SPSOURCE_DIR)/include
CFLAGS = $(C_INCLUDES) -fPIC
COPTS = -O3 -mtune=native
CNOPTS = -O0 -g
LIBS = softposit_math.a $(SPBUILD_DIR)/softposit.a -lm
SOFTPOSIT_OPTS = #-DSOFTPOSIT_EXACT

SOURCE_CODE = $(SOURCE_DIR)/p32/p32_floor.c \
			$(SOURCE_DIR)/p32/p32_ceil.c \
			$(SOURCE_DIR)/p32/p32_abs.c \
			$(SOURCE_DIR)/p32/p32_max.c \
			$(SOURCE_DIR)/p32/p32_min.c \
			$(SOURCE_DIR)/p16/p16_exp.c \
			$(SOURCE_DIR)/p16/p16_exp2.c \
			$(SOURCE_DIR)/p16/p16_log.c \
			$(SOURCE_DIR)/p16/p16_log2.c \
			$(SOURCE_DIR)/p16/p16_sinpi.c \
			$(SOURCE_DIR)/p16/p16_cospi.c \
			$(SOURCE_DIR)/p16/p16_tanpi.c \
			$(SOURCE_DIR)/p16/p16_asinpi.c \
			$(SOURCE_DIR)/p16/p16_acospi.c \
			$(SOURCE_DIR)/p16/p16_atanpi.c \
			$(SOURCE_DIR)/p16/p16_abs.c \
			$(SOURCE_DIR)/p16/p16_floor.c \
			$(SOURCE_DIR)/p16/p16_ceil.c \
			$(SOURCE_DIR)/p16/p16_max.c \
			$(SOURCE_DIR)/p16/p16_min.c \
			$(SOURCE_DIR)/p8/p8_abs.c \
			$(SOURCE_DIR)/p8/p8_exp.c \
			$(SOURCE_DIR)/p8/p8_log.c \
			$(SOURCE_DIR)/p8/p8_floor.c \
			$(SOURCE_DIR)/p8/p8_ceil.c \
			$(SOURCE_DIR)/p8/p8_max.c \
			$(SOURCE_DIR)/p8/p8_min.c \
			$(SOFTPOSIT_OPTS) $(CFLAGS)

all:
	$(CC) -lm -c \
				$(SOURCE_CODE) $(COPTS)
	ar crs softposit_math.a *.o

debug:
	$(CC) -lm -c \
				$(SOURCE_CODE) $(CNOPTS)
	ar crs softposit_math.a *.o

single-lib:
	$(CC) -lm -c \
                                $(SOURCE_CODE) $(COPTS)
	ar crs softposit_math.a *.o $(SPBUILD_DIR)/*.o

check: softposit_math.a
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_exp.exe $(SOURCE_DIR)/tests/test-p16_exp.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_exp2.exe $(SOURCE_DIR)/tests/test-p16_exp2.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_log.exe $(SOURCE_DIR)/tests/test-p16_log.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_log2.exe $(SOURCE_DIR)/tests/test-p16_log2.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_sinpi.exe $(SOURCE_DIR)/tests/test-p16_sinpi.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_cospi.exe $(SOURCE_DIR)/tests/test-p16_cospi.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_tanpi.exe $(SOURCE_DIR)/tests/test-p16_tanpi.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_asinpi.exe $(SOURCE_DIR)/tests/test-p16_asinpi.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_acospi.exe $(SOURCE_DIR)/tests/test-p16_acospi.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(CNOPTS) -o test-p16_atanpi.exe $(SOURCE_DIR)/tests/test-p16_atanpi.c $(LIBS)
	./test-p16_exp.exe
	./test-p16_exp2.exe
	./test-p16_log.exe
	./test-p16_log2.exe
	./test-p16_sinpi.exe
	./test-p16_cospi.exe
	./test-p16_tanpi.exe
	./test-p16_asinpi.exe
	./test-p16_acospi.exe
	./test-p16_atanpi.exe

benchmarks: softposit_math.a
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(COPTS) -fopenmp -o benchmark-p16_exp.exe $(SOURCE_DIR)/benchmarks/benchmark-p16_exp.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(COPTS) -fopenmp -o benchmark-p16_exp2.exe $(SOURCE_DIR)/benchmarks/benchmark-p16_exp2.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(COPTS) -fopenmp -o benchmark-p16_log.exe $(SOURCE_DIR)/benchmarks/benchmark-p16_log.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(COPTS) -fopenmp -o benchmark-p16_log2.exe $(SOURCE_DIR)/benchmarks/benchmark-p16_log2.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(COPTS) -fopenmp -o benchmark-p16_sinpi.exe $(SOURCE_DIR)/benchmarks/benchmark-p16_sinpi.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(COPTS) -fopenmp -o benchmark-p16_cospi.exe $(SOURCE_DIR)/benchmarks/benchmark-p16_cospi.c $(LIBS)
	$(CC) $(SOFTPOSIT_OPTS) $(CFLAGS) $(COPTS) -fopenmp -o benchmark-p16_tanpi.exe $(SOURCE_DIR)/benchmarks/benchmark-p16_tanpi.c $(LIBS)

quad: SOFTPOSIT_OPTS+= -DSOFTPOSIT_QUAD -lquadmath
quad: all

.PHONY = clean

clean:
	$(DELETE) *.o softposit_math.a test-*.exe benchmark-*.exe
