#include "positMath.h"
#include <stdio.h>
#include <omp.h>

int main (int argc, char *argv[]){

    posit16_t pA, pZ;
    double time;
    int i=0, numthreads;
    unsigned long j=0, end;

#pragma omp parallel
#pragma omp master
    numthreads = omp_get_num_threads();

    end = numthreads*24000;

    printf("Benchmarking exponential_2 posit operations using %d threads.\n", numthreads);
    time = omp_get_wtime();

#pragma omp parallel for private(i,pA,pZ)
    for (j=0; j<end; j++) {

		for (i=36160; i<65379; i++)			// 29219 iterations
		{
			pA.v = i;
			pZ = p16_exp2(pA);
		}
		if (numthreads > 65535) printf("Dummy line");	// Prevent compiler optimising out loop
		for (i=221; i<29377; i++)			// 29156 iterations
		{
			pA.v = i;
			pZ = p16_exp2(pA);
		}
		if (numthreads > 65535) printf("Dummy line");	// Prevent compiler optimising out loop

    }
    time = omp_get_wtime() - time;

    printf("Done %lu exponential_2 posit operations in %f seconds, using %d threads.\n", end * 58285, time, numthreads);
    printf("Speed is %.0f pop/s.\n", (double) (end * 58285)/time);

    return 0;

}

