
/*============================================================================

This C source file is part of the SoftPosit Math Package
by S. H. Leong (Cerlane), John Gustafson and Jonathan Low

Copyright 2019 A*STAR.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=============================================================================*/

#include "internals.h"

uint_fast64_t p16_logpoly( uint_fast64_t );

posit16_t p16_log( posit16_t pA ) {

	union ui16_p16 uA;
	uint_fast16_t uiA;
	uint_fast64_t bit, f, sign;
	int_fast64_t s;			// s can be negative
	uA.p = pA;
	uiA = uA.ui;
	f = uiA;

	if ( (f > 0x7FFF) || !f ) {	// if input is 0, or greater than maxpos, return NaR
		uA.ui = 0x8000;
		return uA.p;
	}

	if ( f & 0x4000 ) {		// decode regime
		s = 0;
		while ( f & 0x2000 ) {
			f = f << 1;
			s += 2;
		}
	} else {
		s = -2;
		while  ( !(f & 0x2000) ) {
			f = f << 1;
			s -= 2;
		}
	}

	if ( f & 0x1000 ) s++;		// decode exponent
	f = f & 0xFFF;			// get 12-bit fraction, without hidden bit
	if (f) f = p16_logpoly(f);	// turn fraction into mantissa of logarithm
	f = (((s < 0) ? 64 + s : s) << 30) | f;

	f = (s < 0) ? 0x1000000000 - (((0x1000000000 - f) * 186065280) >> 28) : ((f * 186065279) >> 28);

	sign = f & 0x800000000;
	if (sign) f = 0x1000000000 - f;	// take absolute value of fixed-point result
	if (f < 0x40000000) {		// turn fixed-point into posit format
		if (f) {
			s = 34;
			while ( !(f & 0x20000000) ) {
				f = f << 1;
				s++;
			}
			f = (f ^ 0x60000000) | (( 1 ^ (s & 1)) << 29);
			s = s >> 1;
			bit = 1 << (s - 1);
			if ( f & bit ) {
				if ((f & (bit - 1)) || (f & (bit << 1))) f += bit;
			}
			f = f >> s;
		}
	} else {
		s = 0;
		while ( f > 0x7FFFFFFF ) {
			f = (f & 1) | (f >> 1);
			s++;
		}
		f = f & 0x3FFFFFFF;
		if (s & 1) f = f | 0x40000000;
		s = s >> 1;
		f = (( (uint_fast64_t)0x200000000 << s) - 0x100000000) | f;
		bit = 0x20000 << s;
		if ( f & bit ) {
			if ((f & (bit - 1)) || (f & (bit << 1))) f += bit;
		}
		f = f >> (s + 18);
	}
	if (sign) f = 0x10000 - f;	// restore sign
	uA.ui = f;
	return uA.p;

}


uint_fast64_t p16_logpoly( uint_fast64_t f ) {

	uint_fast64_t s, z, zsq;

	z = ((f << 31) + 2) / (f + 8192);	// fixed-point divide; discard remainder
	zsq = (( z * z ) >> 30);		// fixed-point squaring
	s = (zsq * 1584) >> 28;
	s = (zsq * (26661 + s)) >> 29;
	s = (zsq * (302676 + s)) >> 27;
	s = (zsq * (16136153 + s)) >> 30;
	s = (z   * (193635259 + s)) >> 27;

	return s;

}
